require('dotenv').config()

const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
  networks: {
    mainnet: {
      provider: function() { 
       return new HDWalletProvider(process.env.MAINNET_XPRIV, process.env.MAINNET_RPC_ENDPOINT);
      },
      network_id: 1,
      gas: 4612388,
      gasPrice: process.env.MAINNET_GAS_PRICE_GWEI // Check current gas price prior to deployment.
    },
    test: {
      host: "localhost",
      port: 9545,
      network_id: "*" // Match any network id,
    },
    development: {
      host: "localhost",
      port: 7545,
      network_id: "*" // Match any network id,
    },
    goerli: {
      provider: function() { 
       return new HDWalletProvider(process.env.GOERLI_XPRIV, process.env.GOERLI_RPC_ENDPOINT);
      },
      network_id: 5,
      gas: 4612388,
      gasPrice: process.env.GOERLI_GAS_PRICE_GWEI // Check current gas price prior to deployment.
    },
  },
  compilers: {
    solc: {
      // prevent truffle / solidity version mismatch.
      version: "^0.4.24",
    }
  }
};
