# Minds Token

## Deployment

Make sure to configure .env file to include the correct XPRIV and RPC endpoints for the network that you are deploying to.

## Goerli Testnet


- Token: [0x0a180bEa9cA0fb7c0Ed15989A803E72f1F044c79](https://goerli.etherscan.io/address/0x0a180bEa9cA0fb7c0Ed15989A803E72f1F044c79)
- Wire: [0xe4835121526b8Eecf5db94f4f270dFF4aA3b562b](https://goerli.etherscan.io/address/0xe4835121526b8Eecf5db94f4f270dFF4aA3b562b)
- Boost: [0x7a086dfAcd97FF7Bff259978A8362b55559Da656](https://goerli.etherscan.io/address/0x7a086dfAcd97FF7Bff259978A8362b55559Da656)
- Withdrawal: [0x21e143db5bF4Fc596289320fA37f3F11de2EA8ea](https://goerli.etherscan.io/address/0x21e143db5bF4Fc596289320fA37f3F11de2EA8ea)

_Copyright Minds 2018_
